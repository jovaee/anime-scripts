Anime Scripts is a collection of small scripts used to manange my anime library. Each script might have or not have a dedicated readme file that explains it in further detail.


# Setup
Run the `init.sh` to copy the `regexes.py` file to the scripts that require it as an import.


# The Scripts

> ## Anime Renamer
Renames anime files according to regexes defined in `regexes.py`.

> ## Anime Grouper
Groups anime files in folders by anime titles by using the regexes defined in `regexes.py`.

> ## Remux
Remuxes video files to MKV containers. Change file matching pattern in the bash file.

> ## AniDB Poster Downloader
Download anime poster from AniDB and save them in the corresponding anime folders. Check the `anidb_poster.py` file for more details.

> ## Missing Duplicate
This assumes that anime are sorted by show. Check for any missing and duplicate episodes in a folder. If a AniDB ID is present in the path that will be used to fetch the official episode count, if not then the maximum episode in the folder is used instead to check for missing episodes.
