"""
Download anime posters from AniDB. This is mainly for Plex to use these instead of using the ones it thinks are best.

Scans Anime library and downloads the posters into the folders with name "poster.<ext>"
It expects the folder structure to be as follows:

- ROOT
    - <name>  # A collection name for the animne
        - <anime_name> [anidb-<id>]  # An anime entry which contains episodes
            - <anime_name> - 01.<ext>
            - <anime_name> - 02.<ext>
            - <anime_name> - 03.<ext>
        - <another_anime_name> [anidb-<id>]    # Another anime entry which contains episodes
            - <another_anime_name> - 01.<ext>
            - <another_anime_name> - 02.<ext>
            - <another_anime_name> - 03.<ext>
        - Other  # A folder that contains other entries likes Movies, OVAs...
            - <anime_movie_name> [anidb-<id>]  # An anime entry which contains episodes
                - <anime_movie_name>.<ext>
"""
import argparse
import json
import logging
import os
import sys
import time
import re

import requests

from logging import handlers
from urllib.request import urlretrieve
from typing import Dict, Union, Optional
from xml.etree import ElementTree

LOG_SIZE = 10  # MB
FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"

API_URL = "http://api.anidb.net:9001/httpapi?request=anime&client={client}&clientver={clientver}&protover=1&aid={aid}"
IMAGE_URL = "https://cdn.anidb.net/images/main/{filename}"
DEFAULT_CONFIG = {
    "client_name": "",
    "client_version": 1,
}

# Setup default logging config
std_handler = logging.StreamHandler(sys.stdout)
file_handler = handlers.RotatingFileHandler("poster.log", maxBytes=LOG_SIZE * 1000 ** 2)

logging.basicConfig(format=FORMAT, level=logging.INFO, handlers=[std_handler, file_handler])
logger = logging.getLogger(__name__)

config = dict()


def make_anidb_request(aid: int) -> Dict:
    """
    """
    url = API_URL.format(**{
        "client": config["client_name"],
        "clientver": config["client_version"],
        "aid": aid
    })

    response = requests.get(url=url)
    return response.content


def get_image_name_from_xml(data: Union[bytes, str]) -> Optional[str]:
    """
    """
    xml_doc = ElementTree.fromstring(data)
    for child in xml_doc:
        if child.tag == 'picture':
            return child.text


def download_poster(filename: str, directory: str):
    """
    """
    url = IMAGE_URL.format(**{"filename": filename})
    response = requests.get(url=url)
    
    
    with open(os.path.join(directory, "poster.jpg"), 'wb') as poster:
        poster.write(response.content)


def process_entry(path: str, force: bool = False):
    """
    """
    folder_name = os.path.basename(path)
    matches = re.match(r".+? \[anidb-(?P<id>\d+)\]", folder_name)
    poster_exists = os.path.exists(os.path.join(path, 'poster.jpg'))

    if matches and (not poster_exists or force):
        response = make_anidb_request(matches["id"])
        image_name = get_image_name_from_xml(response)
        
        logger.info("Downloading poster for \"%s\"...", folder_name)
        download_poster(image_name, path)
        
        # Throttle so not to get banned again
        time.sleep(2)
    else:
        if not matches:
            logger.info("Skipping \"%s\" as it does not match expected naming convention", folder_name)
        elif poster_exists:
            logger.info("Skipping \"%s\" as it already has a poster file", folder_name)


def process_collection(root: str, force: bool = False):
    """
    """
    for collection in os.listdir(root):  # Go through ROOT (collections)
        collection_path = os.path.join(root, collection)
        if os.path.isdir(collection_path):
            for entry in os.listdir(collection_path):  # Go through collection entries
                entry_path = os.path.join(collection_path, entry)
                if os.path.isdir(entry_path):
                    if entry == "Other":
                        for other_entry in os.listdir(entry_path):  # Go through the Other entries
                            other_entry_path = os.path.join(entry_path, other_entry)
                            if os.path.isdir(other_entry_path):
                                process_entry(other_entry_path, force=force)
                    else :
                        process_entry(entry_path, force=force)

def main(root: str):
    """
    """
    global config

    # Check that config.json exists
    path = os.path.dirname(os.path.realpath(__file__))
    config_path = os.path.join(path, 'config.json')
    if not os.path.isfile(config_path):
        logger.info('Config file does not exist. Creating...')
        with open(config_path, 'w', encoding='utf-8') as config_file:
            json.dump(DEFAULT_CONFIG, config_file, indent=4)

        logger.info('Config file created. Please fill in config details and run again')
        sys.exit(0)
    else:
        with open(config_path, 'r', encoding='utf-8') as config_file:
            logger.info('Reading config file...')
            config = json.load(config_file)
    
    logger.info("Searching for anime in \"%s\"...", root)
    process_collection(root)    


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Download anime posters")
    parser.add_argument("--source", "-s", type=str, required=True, help="Source directory from where to search for anime")

    args = parser.parse_args()
    if not os.path.exists(args.source):
        logger.error('The source path you have provided does not exist')
        sys.exit(0)

    main(args.source)
