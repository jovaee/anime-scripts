FROM python:3.10-slim

# Set path for log file
ENV LOG_PATH="manager" 

RUN apt update && apt install inotify-tools sudo -y
RUN pip install -U pip

# Copy only the required files at root level
COPY regexes.py init.sh ./
COPY _shared ./_shared
# Copy the manager files
COPY manager/ manager/

RUN ./init.sh

RUN pip install -r manager/requirements.txt

CMD ["bash", "manager/inotify.sh"]
