REGEXES = [
    # Check https://regex101.com/r/y7DHLQ/14 for regex tester
    r'\[(?P<subber>HorribleSubs)\]\s(?P<title>.+?)(?:\s-\s(?P<episode>[0-9]{1,4}(?:\.5|[A-Z])?)(?:v\d)?)?\s\[(?P<resolution>\d+p)\].(?P<extension>.+)',
    # Check https://regex101.com/r/eKaXpp/5 for regex tester
    r'\[(?P<subber>Erai-raws)\]\s(?P<title>.+?)(?:\s-\s(?P<episode>[0-9]{1,4}(?:\.5|[A-Z])?)(?:\sEND|v\d)?)?\s(?:\[v\d\])?\[(?P<resolution>\d+p)\].*\.(?P<extension>.+)',
    # Check https://regex101.com/r/oOq5kC/4 for regex tester
    r'\[(?P<subber>Judas)\]\s(?P<title>.+?)(?:\s-\s(?P<episode>[0-9]{1,4}(?:\.5|[A-Z])?)(?:\sv\d)?)?\s\[(?P<resolution>\d+p)\].*\.(?P<extension>.+)',
    # Check https://regex101.com/r/5BckNU/2 for regex tester
    r'\[(?P<subber>SubsPlease)\]\s(?P<title>.+?)(?:\s-\s(?P<episode>[0-9]{1,4}(?:\.5)?)(?:v[0-9]{1})?)?\s\((?P<resolution>\d+p)\).*\.(?P<extension>.+)',
]