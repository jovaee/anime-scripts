import os
import re
import sys
import time

import requests

from collections import Counter
from typing import Callable, Dict, List, Optional, Union
from xml.etree import ElementTree

FILE_REGEX = r"\[(?P<subber>.+)\]\s(?P<title>.+?)(?:\s-\s(?P<episode>(?:S?[0-9]{1,4}(?:\.5|[A-Z])?\-?){1,2}))?\s\[(?P<resolution>\d+p)\].(?P<extension>.+)"
FOLDER_REGEX = r"\[anidb-(?P<aid>[0-9]+)\]"
ANIDB_API_URL = "http://api.anidb.net:9001/httpapi?request=anime&client={client}&clientver={clientver}&protover=1&aid={aid}"

CLIENT_NAME = os.environ.get("ANIDB_CLIENT_NAME")
CLIENT_VERSION = os.environ.get("ANIDB_CLIENT_VERSION", 1)


def rate_limit(func: Callable) -> Callable:
    """
    Rate limit a function and make sure it's isn't called more than once every x milliseconds
    """
    milliseconds = 3000
    last_execute_time = 0

    def inner(*args, **kwargs):
        nonlocal last_execute_time

        now = time.time()
        if now - last_execute_time < milliseconds:
            time.sleep((milliseconds - (now - last_execute_time) + 100) // 1000)  # Just add a bit of a buffer to be safe

        res = func(*args, **kwargs)
        last_execute_time = time.time()

        return res

    return inner


@rate_limit  # Rate limit calls to AniDB so that we don't get banned again :o
def get_anidb_media(anidb_id: int) -> Dict:
    """
    Make the AniDB API call
    """
    url = ANIDB_API_URL.format(client=CLIENT_NAME, clientver=CLIENT_VERSION, aid=anidb_id)
    
    response = requests.get(url=url)
    return response.content


def get_official_episode_count(anidb_id: int) -> Optional[int]:
    """
    Get the official episode count from AniDB
    """
    data = get_anidb_media(anidb_id)

    xml_doc = ElementTree.fromstring(data)
    episode_count = xml_doc.find("episodecount").text

    return int(episode_count) if episode_count else None


def print_output(path: str, duplicates: List[int], missing: List[int], unknown: List[str]):
    """
    Output findings for a specific folder
    """
    if duplicates or missing or unknown:
        duplicates_str = "\nDuplicates:\n" + "\n".join(f"  > {episode}" for episode in sorted(duplicates))
        missing_str = "\nMissing:\n" + "\n".join(f"  > {episode}" for episode in sorted(missing))
        unknown_str = "\nUnknown:\n" + "\n".join(f"  > {episode}" for episode in unknown)

        print(f"====================\n{path}\n--------------------{duplicates_str if duplicates else ''}{missing_str if missing else ''}{unknown_str if unknown else ''}\n++++++++++++++++++++")


def check_duplicate_episodes(episodes: List[int]) -> List[int]:
    """
    Get a list of duplicate episodes
    """
    counter = Counter(episodes)
    
    duplicates: List[int] = [episode for episode, count in counter.items() if count > 1]
    return duplicates


def check_missing_episodes(episodes: List[int], official_episode_count: Optional[int]) -> List[int]:
    """
    Get a list of missing episodes
    """
    missing_episodes: List[int] = []

    if episodes:
        unique_episodes = set(episodes)
        min_episode = 1  # min(episodes)
        max_episode = official_episode_count if official_episode_count else max(episodes) 
        all_episodes = set(range(min_episode, max_episode + 1))

        missing_episodes = list(all_episodes - unique_episodes)
    
    return missing_episodes


def process_files(path: str, files: List[str]) -> Union[List[int], List[int]]:
    """
    Process files and check for missing and duplicate episodes
    """
    episodes: List[int] = []
    unknown: List[str] = []

    # If the path has a anidb ID in it then we can get an offical episode count we can use
    official_episode_count = None
    match = re.search(FOLDER_REGEX, path)
    if match:
        official_episode_count = get_official_episode_count(match["aid"])

    for file in files:
        match = re.match(FILE_REGEX, file)
        # If no episode then it's a movie and usually there is only one then
        # TODO: This doesn't currently support any of the more advanced episode formats like S01, 501-503 etc
        # These are usually quite rare and them poluting the output isn't that big of a deal
        # Might implement support for them in the future if I feel like it
        if match and match["episode"] is not None:
            try:
                episode = int(match["episode"])
                episodes.append(episode)
            except (TypeError, ValueError):
                unknown.append(file)

    duplicates = check_duplicate_episodes(episodes)
    missing = check_missing_episodes(episodes, official_episode_count)

    return duplicates, missing, unknown


def traverse_directories(path: str):
    """
    Recursively go through all folder and check for files
    """
    subpaths: List[str] = os.listdir(path)
    files: List[str] = [p for p in subpaths if os.path.isfile(os.path.join(path, p))]
    child_dirs: List[str] = [p for p in subpaths if os.path.isdir(os.path.join(path, p))]

    if files:
        duplicates, missing, unknown = process_files(path, files)
        print_output(path, duplicates, missing, unknown)

    for cd in child_dirs:
        traverse_directories(os.path.join(path, cd))


def main():
    """
    Main
    """
    if not CLIENT_NAME or not CLIENT_VERSION:
        print("Please set ANIDB_CLIENT_NAME and ANIDB_CLIENT_VERSION environment variables")
        exit(1)

    traverse_directories(sys.argv[1])


if __name__ == "__main__":
    main()