ROOT="/mnt/user/Downloads/Complete/"  

## READ
# Remove -v quiet from the docker run command to see errors if files aren't getting remuxed

# When going to remux AVI files add '-fflags +genpts' to the docker run.
# AVI files don't have internal timestamps which are required for MKV
# By adding these flags ffmpeg will automatically add the flags

# It's possible for remuxing to fail if the subtitle format used by
# the original container is not supported in a MKV container.
# An error like this could be raised:
# [matroska @ 0x9db200] Subtitle codec 94213 is not supported.
# Add -c:s srt after -map 0 -c copy in the docker run command
# to convert the subtiles into a subrip format to allow the MKV
# container to retain the subtitles. It's advised to disable
# deleting of original files so that it can be checked if the
# remuxing worked as intented.
##

set -e
find "$ROOT" -type f -iname '*.mp4' |
	while read fname; do
		# The path that the file would be accisable from the docker container
		# Remove $ROOT from file as it's the mount path for the docker image
		CONTAINER_FILE_PATH=${fname#"$ROOT"}  # Includes filename
		CONTAINER_PATH=`dirname "$CONTAINER_FILE_PATH"`  # Does not include filename
		# Just the filename
		FILENAME=${fname##*/}
		# Filename without the file extension
		FILENAME_EXTENSIONLESS=${FILENAME%.*}
		# Just the directory name on the host for the file
		DIRNAME=`dirname "$fname"`

		echo "  =========="
		echo "| $FILENAME"
		echo "| ----------"
		echo "| Source Host Path: ${DIRNAME}"
		echo "| Source Container Path: ${CONTAINER_PATH}"

		# Remux the file into a MKV container
		docker run --rm -v "${ROOT}:/Content" jrottenberg/ffmpeg -v quiet -stats -y -i "/Content/${CONTAINER_FILE_PATH}" -map 0 -c copy "/Content/${CONTAINER_PATH}/${FILENAME_EXTENSIONLESS}.mkv"
		if [ $? -ne 0 ]
		then
			echo "Something failed"
			exit $?
		fi

		# Remove the original file because it's no longer required
		echo "| ---------"
		echo "| Remuxing complete"
		echo "| Deleting $fname"
		echo "  ========="
		echo ""
		rm "$fname"
	done
