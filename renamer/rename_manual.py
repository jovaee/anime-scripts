"""
Writes all files to a text file which can be used to bulk rename files

NOTE: This was written on Python2
"""
import os
import sys

# NOTE: Appears that listdir returns the files in order how the filesystem indexed them
path = sys.argv[1]
files = [thing for thing in os.listdir(path) if os.path.isfile(os.path.join(path, thing))]
if len(files) == 0:
	print('No files in directory')
	sys.exit(1)

with open('files.txt', 'w') as file:
    for f in files:
        file.write(f + '\n')

ans = raw_input('Please press any key to continue (C to cancel)')
if ans == 'c' or ans == 'C':
    sys.exit(1)

with open('files.txt', 'r') as file:
    new_names = file.readlines()
    if len(new_names) != len(files):
    	print('You have %s files but %s names in files.txt. The number should be the same' % (len(new_names), len(files)))

    for i, new_name in enumerate(new_names):
        # TODO: Log this to a file BECAUSE
        print('Renaming %s to %s' % (files[i], new_name.strip()))
        os.rename(os.path.join(path, files[i]), os.path.join(path, new_name.strip()))
