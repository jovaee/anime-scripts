"""
Anime renamer

Renames files using regexes to extract information from the original file names
"""
import argparse
import os
import re
import sys

from shutil import move

from regexes import REGEXES


def main(source: str):
    if not os.path.exists(source):
        print('The source path you have provided does not exist')
        sys.exit(0)

    for item in list(os.listdir(source)):
        item_path = os.path.join(source, item)
        if not os.path.isfile(item_path):
             main(item_path)

        path = os.path.dirname(item_path)
        filename = os.path.basename(item_path)

        for regex in REGEXES:
            matches = re.match(regex, filename)
            if matches:
                if matches['episode'] is not None:
                    renamed = f"[{matches['subber']}] {matches['title']} - {matches['episode']} [{matches['resolution']}].{matches['extension']}"
                else:
                    renamed = f"[{matches['subber']}] {matches['title']} [{matches['resolution']}].{matches['extension']}"

                # No need to rename file if it already matches desired format
                if renamed != filename:
                    os.rename(item_path, os.path.join(path, renamed))
                    print(f'Renamed "{filename}" to "{renamed}" in folder "{path}"')
                
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Rename anime")
    parser.add_argument("--source", "-s", type=str, required=True, help="Source directory from where to search for anime")

    args = parser.parse_args()

    main(args.source)
