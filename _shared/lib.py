import time

from typing import Callable


def rate_limit(func: Callable) -> Callable:
    """
    Rate limit a function and make sure it's isn't called more than once every x milliseconds
    """
    milliseconds = 3000
    last_execute_time = 0

    def inner(*args, **kwargs):
        nonlocal last_execute_time

        now = time.time()
        if now - last_execute_time < milliseconds:
            time.sleep((milliseconds - (now - last_execute_time) + 100) // 1000)  # Just add a bit of a jitter to be safe

        res = func(*args, **kwargs)
        last_execute_time = time.time()

        return res

    return inner