import os
import requests
import time

from datetime import datetime
from typing import Optional
from xml.etree import ElementTree

from lib import rate_limit

ANIDB_API_URL = "http://api.anidb.net:9001/httpapi?request=anime&client={client}&clientver={clientver}&protover=1&aid={aid}"

CLIENT_NAME = os.environ.get("ANIDB_CLIENT_NAME")
CLIENT_VERSION = os.environ.get("ANIDB_CLIENT_VERSION", 1)

CONFIG_DIR = os.environ.get("CONFIG_DIR", "/config/")  # / at end of default otherwise next line goes to root instead of /config
FULL_CONFIG_DIR = os.path.dirname(CONFIG_DIR)


# Rate limit calls to AniDB so that we don't get banned again :o
# Banning can happen for other reasons as well though
@rate_limit  
def fetch_anidb_media(anidb_id: int) -> bytes:
    """
    Make the AniDB API call to get data document
    """
    url = ANIDB_API_URL.format(client=CLIENT_NAME, clientver=CLIENT_VERSION, aid=anidb_id)
    return requests.get(url=url).content


def save_anidb_cached_data(anidb_id: int, xml_data: bytes):
    """
    Save AniDB anime data to cache file
    """
    path = os.path.join(FULL_CONFIG_DIR, 'cache', '%s.xml' % anidb_id)
    if os.path.exists(os.path.join(FULL_CONFIG_DIR, 'cache')):
        with open(path, 'wb') as xml_file:
            xml_file.write(xml_data)


def get_anidb_media(anidb_id: int, max_cache_age: int = 60 * 60 * 24 * 7) -> ElementTree:
    """
    Get the anidb document
        1) From cache if still valid
        2) From anidb api
    """
    path = os.path.join(FULL_CONFIG_DIR, 'cache', '%s.xml' % anidb_id)
    if os.path.exists(path) and time.time() - os.path.getmtime(path) < max_cache_age:
        with open(path, 'r', encoding="utf-8") as xml_file:
            return ElementTree.fromstring(xml_file.read())
    else:
        raw_data = fetch_anidb_media(anidb_id)
        xml_doc = ElementTree.fromstring(raw_data)

        # Check this is not a ban response... again
        if xml_doc.attrib.get("code", 200) == 200:
            save_anidb_cached_data(anidb_id, raw_data)
        elif not globals().get("ban_printed"):  # Only print once per run
            print(f"ERROR: Getting **{xml_doc.attrib['code']} - {xml_doc.text}** responses from AniDB. AniDB related logic might not work as intended")
            globals()["ban_printed"] = True
        
        return xml_doc


def get_official_episode_count(anidb_id: int) -> Optional[int]:
    """
    Get the official episode count from AniDB
    """
    xml_doc = get_anidb_media(anidb_id)
    episode_count = xml_doc.find("episodecount")
    if episode_count is not None:
        return int(episode_count.text)


def get_official_end_date(anidb_id: int) -> Optional[datetime]:
    """
    Get the official end date from AniDB
    """
    xml_doc = get_anidb_media(anidb_id)
    end_date = xml_doc.find("enddate")
    if end_date is not None:
        return datetime.strptime(end_date.text, "%Y-%m-%d")
