#!/bin/sh

# Copy regexes.py to all scripts that require it as an import
requiredRegex="manager renamer grouper"

for f in $requiredRegex; do
    if [ -d "$f" ]; then
        echo "Copying regexes.py to /$f"
        cp -f regexes.py $f/regexes.py
    fi
done

# Copy shared lib files to all that require it
requiredShared="missing_duplicate manager"

for f in $requiredShared; do
    if [ -d "$f" ]; then
        echo "Copying shared libs to /$f"
        cp -rf _shared/* $f
    fi
done