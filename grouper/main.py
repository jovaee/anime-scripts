"""
Anime Grouper
"""
import argparse
import logging
import os
import re
import sys

from collections import defaultdict
from logging import handlers
from shutil import move

from regexes import REGEXES

LOG_SIZE = 10  # MB
FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"

# Setup default logging config
std_handler = logging.StreamHandler(sys.stdout)
file_handler = handlers.RotatingFileHandler("grouper.log", maxBytes=LOG_SIZE * 1000 ** 2)

logging.basicConfig(format=FORMAT, level=logging.INFO, handlers=[std_handler, file_handler])
logger = logging.getLogger(__name__)


def main(source: str):
    groups = defaultdict(list)
    
    files = [f for f in os.listdir(source) if os.path.isfile(os.path.join(source, f))]
    
    for f in files:
        matched = False
        for regex in REGEXES:
            matches = re.match(regex, f)
            if matches:
                groups[matches['title']].append(f)
                matched = True
                break

        if matched is False:
            logger.info('Skipping "%s" as it did not match any regex', f)

    for name, files in groups.items():
        # Unraid doesn't like the . at the end of the folder name when created with this script, so we remove
        # it from the anime name
        if name[-1] == '.':
            name = name[:-1]
        
        # Check if a folder does not already exist for this name and if it does not we create it
        folder_name = os.path.join(source, name)
        if not os.path.exists(folder_name):
            logger.info('Creating folder for "%s"', name)
            os.mkdir(folder_name)

        # Now move all the files under this name to the folder
        for f in files:
            # Just make sure that a file with this name does not already exist in the destination folder
            # We skip these to prevent accidental data loss
            if os.path.exists(os.path.join(folder_name, f)):
                logger.info('Skipping "%s" as it already exists in folder "%s"', f, name)
                continue

            move(os.path.join(source, f), os.path.join(folder_name, f))
            logger.info('Successfully moved file "%s" to folder "%s"', f, name)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Group anime by name")
    parser.add_argument("--source", "-s", type=str, required=True, help="Source directory from where to search for anime")

    args = parser.parse_args()
    if not os.path.exists(args.source):
        logger.error('The source path you have provided does not exist')
        sys.exit(0)

    try:
        main(args.source)
    except Exception:
        logger.exception('An exception occurred')
    else:
        logger.info('Successfully ran anime-renamer')
