#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# -E preserve env variables under other user
# -u user to run command as
# -g user group to run command as
sudo -E -u "#$PUID" -g "#$PGID" python3 $DIR/validate.py