"""
A mapping has the following structure

{
    'anidb_id': 5,
    'collection_name': 'Boku no Hero Academia',
    'required_episode_title': 'Boku no Hero Academia',
    'required_episode_range': range(1, 25),
    'episode_transform': lambda x: x,
    'title_transform': 'New Boku no Hero Academia Title',
    'destination_folder_name': 'Boku no Hero Academia',
}

The fields are as follows:
    - anidb_id: The AniDB ID for the anime. This will only be used to create the destination_folder_name folder if it doesn't exist yet. This is to allow for the format "Boku no Hero Academia [anidb-5]"
    - collection_name: The collection name under which this anime entry lies. The collection is suppose to be a logical collection of prequels, sequels and specials.
                       This cannot end with a '.'. It simply gets removed and then the path doesn't match anymore
    - required_episode_title: The title that the file should have to match this mapping entry. Can be omitted and will fallback to 'entry_folder_name' and 'collection_name' in that order
    - required_episode_range: The episode number the the file episode should have to match this mapping entry. Specified in format (start_episode, end_episode)
    - episode_transform: A transformation function that can change the episode number to something else if required. Note that this only works for the numeric part of the episode.
                         Special cases like '01A', the 'A' will be removed before sending into the transformation function. Same with something like '201-205'. Each number will be
                         sent in separately and will be combined afterwards. (Optional)
    - title_transform: A static value or transformation function that can change the title of the filename. Normally this would just return a new string, but more complex stuff is possible. (Optional)
    - entry_folder_name: The destination folder name inside the collection_name folder. Can be omitted and will fallback to 'collection_name'
    - other: Is this mapping regarded as an Other eg Special, OVA etc. This will follow the same mapping logic but just put the moved files under a sub entry folder called Other. (Optional)
"""
def ep_from_offset(episode: str | int, offset: int, zfill_size: int=2) -> str:
    """
    Used with 'episode_transform'. Get the episode that should be used in the filename with a given offset
    reset_episode_start(50, 49, 2) -> "01"
    reset_episode_start(101, 49, 3) -> "052"
    """
    return f"{int(episode) - offset}".zfill(zfill_size)


MAPPINGS = [
    {
        'anidb_id': 14416,
        'collection_name': 'Boku no Hero Academia',
        'entry_folder_name': 'Boku no Hero Academia 4th Season',
        'required_episode_range': (1, 24),
    },
]