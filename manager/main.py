import click
import logging
import os
import re
import sys
import shutil

from logging import handlers
from types import FunctionType
from typing import Match

from mappings import MAPPINGS
from regexes import REGEXES
from rulesets import SPARSE

LOG_SIZE = 10  # MB
FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"

LOG_PATH = os.getenv("LOG_PATH", ".")
SKIP_HALF_EPISODES = os.getenv("SKIP_HALF_EPISODES") == "true"
DELETE_HALF_EPISODES = os.getenv("DELETE_HALF_EPISODES") == "true"

# Set umask for newly created folders
umask = int(os.getenv("UMASK", '002'), 8)
os.umask(umask)

# Setup default logging config
std_handler = logging.StreamHandler(sys.stdout)
file_handler = handlers.RotatingFileHandler(
    os.path.join(LOG_PATH, "manager.log"),
    backupCount=5,
    maxBytes=LOG_SIZE * 1000 ** 2,
)

logging.basicConfig(format=FORMAT, level=logging.INFO, handlers=[std_handler, file_handler])
logger = logging.getLogger("manager")


def frange(start: int | float, stop: int | float, step: int | float) -> list[float]:
    steps = []
    r = float(start)
    while r < stop:
        steps.append(r)
        r += step

    return steps


def move_matched_file(src: str, dest: str, mapping: dict, new_filename: str):
    """
    Move file from src to dest based on mapping information specified
    """
    collection_name = mapping['collection_name']
    entry_folder_name = mapping.get('entry_folder_name', collection_name)
    is_other = mapping.get('other') is True

    collection_path = os.path.join(dest, collection_name)
    other_path = 'Other' if is_other else ''  # Nest entry in Other/
    entry_path = os.path.join(collection_path, other_path, entry_folder_name)
    entry_aid_path = os.path.join(collection_path, other_path, f"{entry_folder_name} [anidb-{mapping['anidb_id']}]")
    
    # Make sure that the overarching collection folder exists
    if not os.path.exists(collection_path):
        logger.info('Creating collection folder "%s"', collection_name)
        os.mkdir(collection_path)

    # Check that the entry folder does not already exist but without the AniDB ID present
    if os.path.exists(entry_path):
        logger.warning(
            'There already exists an entry for "%s" without AniDB ID present. Skipping file',
            entry_folder_name
        )
        return

    # Make sure that the entry folder with AniDB ID present exists
    if not os.path.exists(entry_aid_path):
        logger.info('Creating entry folder "%s" for collection "%s"', entry_folder_name, collection_name)
        os.makedirs(entry_aid_path)

    final_path = os.path.join(entry_aid_path, new_filename)
    if sys.platform == 'win32' and len(final_path) > 260:
        logger.error('Destination path exceeds the max lengths which Windows supports. Skipping file "%s"', src)
        return

    logger.info('Moving "%s" to "%s"', src, final_path)
    shutil.move(src, final_path)


def find_matching_regex(filename: str) -> dict | None:
    """
    Find regex that matches given filename
    """
    for regex in REGEXES:
        match = re.match(regex, filename)
        if match:
            return match


def generate_episode_range(required_episode_range: list | set | tuple) -> list[float]:
    """
    Get list of episode numbers from 'start' to 'end'
    """
    start, end = required_episode_range[0], required_episode_range[1]
    return frange(start, end + 0.5, 0.5)


def find_matching_mapping(group: Match, filename: str) -> dict | None:
    """
    Find mapping which matches given group
    """
    for mapping in MAPPINGS:
        if group['episode'] is None:
            # TODO: Currently supporting stuff without episodes is not a thing. This is mostly just movies
            return
        
        # If a specified episode title is given use that
        if 'required_episode_title' in mapping:
            required_title = mapping['required_episode_title']
            logger.debug(f'Using required_episode_title "{mapping["required_episode_title"]}" for title mapping match')
        # Otherwise fallback to the entry folder name if present
        elif 'entry_folder_name' in mapping:
            required_title = mapping['entry_folder_name']
            logger.debug(f'Using entry_folder_name "{mapping["entry_folder_name"]}" for title mapping match')
        # Otherwise use the collection name which is always available
        else:
            required_title = mapping['collection_name']
            logger.debug(f'Using collection_name "{mapping["collection_name"]}" for title mapping match')

        # Use the precomputed episode range from a previous comparison if possible
        if mapping.get("episode_range_generated", False):
            episode_range =  mapping["episode_range"]
        else:
            episode_range = generate_episode_range(mapping['required_episode_range'])
            mapping["episode_range"] = episode_range
            mapping["episode_range_generated"] = True

        if required_title == group['title']:
            # Perform any special actions against the group episode
            match = re.match(r'(\d+)([a-zA-Z]+)', group['episode'])  # Something like 01A
            if match and int(match[0]) in episode_range:
                return mapping
            match = re.match(r'(\d+)\-(\d+)', group['episode'])  # Something like 01-05
            if match and int(match[0]) in episode_range and int(match[1]) in episode_range:
                return mapping
            if float(group['episode']) in episode_range:  # Anything else like x or x.5
                return mapping

            logger.warning('"%s" had a mapping but did not meet episode range requirements', filename)


def discard_unwanted(match: Match, src: str) -> bool:
    """
    Discard unwanted episodes based on rules and set flags
    """
    if float(match['episode']) % 1 == 0.5:
        # x.5 episodes should just be skipped
        if SKIP_HALF_EPISODES:
            logger.info(f'Skipping "{src}" as it is a half episode')
            return True

        # x.5 episodes should be deleted
        if DELETE_HALF_EPISODES:
            logger.info(f'Deleting "{src}" as it is a half episode')
            os.remove(src)
            return True

    return False


def get_new_filename(match: Match, mapping: dict) -> str:
    """
    Get the filename for the file based on the mapping
    """
    # New title will be chosen in the following order based on mapping values
    # 1 title_transform
    # 2 entry_folder_name
    # 3 collection_name
    title = match['title']
    if 'title_transform' in mapping:
        if isinstance(mapping['title_transform'], FunctionType):
            title = mapping['title_transform'](title)
        else:
            title = mapping['title_transform']
    elif 'entry_folder_name' in mapping:
        title = mapping['entry_folder_name']
    elif 'collection_name' in mapping:
        title = mapping['collection_name']
    else:
        # Technically shouldn't be able to get here with the validation in place
        raise RuntimeError('Unable to determine new filename. Mapping not defined correctly')

    episode = match['episode']
    if 'episode_transform' in mapping:
        # TODO: Do the same thing as in find_matching_mapping
        episode = mapping['episode_transform'](episode)
    
    return '[{subber}] {title} - {episode} [{resolution}].{extension}'.format(
        **{
            'subber': match['subber'],
            'title': title,
            'episode': episode,
            'resolution': match['resolution'],
            'extension': match['extension'],
        }
    )


def process_source_file(src: str, dest: str):
    """
    Processes a given file and move it to dest if required
    """
    filename = os.path.basename(src)

    match = find_matching_regex(filename)
    if match:
        # Check if file matches any of the mappings
        mapping = find_matching_mapping(match, filename)
        if mapping:
            # Discard/Skip any unwanted episodes
            discarded = discard_unwanted(match, src)
            if discarded:
                 # If the files were discarded or skipped stop processing
                return

            # Make sure the mapping is valid
            feedback = SPARSE.validate(mapping)
            if feedback:
                logger.error("The matching mapping failed validation")
                logger.error({f"#{mapping.get('anidb_id', src)}": feedback})
                return

            new_filename = get_new_filename(match, mapping)

            if filename != new_filename:
                logger.info('Renaming "%s" to "%s"', filename, new_filename)

            move_matched_file(src, dest, mapping, new_filename)


def process_source_folder(src: str, dest: str):
    """
    Process all folders and files in src
    """
    listdirs = os.listdir(src)
    folders = [ld for ld in listdirs if os.path.isdir(os.path.join(src, ld))]
    files = [ld for ld in listdirs if os.path.isfile(os.path.join(src, ld))]
    for file in files:
        process_source_file(os.path.join(src, file), dest)

    for folder in folders:
        process_source_folder(os.path.join(src, folder), dest)


@click.command("manager")
@click.option(
    "--source", "-s",
    type=click.Path(exists=True, readable=True),
    required=True,
    help="Source directory from where to search for anime"
)
@click.option(
    "--destination", "-d",
    type=click.Path(exists=True, writable=True),
    required=True,
    help="The destination folder to where anime will be moved. This should be the root of your collection's folder"
)
def main(source: str, destination: str):
    """
    """
    try:
        logger.info('Running anime-manager...')
        process_source_folder(source, destination)
    except Exception:
        logger.exception('An exception occurred')
    else:
        logger.info('Successfully ran anime-manager')


if __name__ == "__main__":
    main()
