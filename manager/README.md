## New filename format

Files are saved in the following format when moved to the `/destination` folder.
```
[{subber}] {title} - {episode} [{resolution}].{extension}
```
- `subber` - Taken as is from the original filename
- `title` - This can be 3 potential values based on the mapping defined. If the action is not defined in the mapping it will fallback to the next option. Choices in descending priority:
    - If `title_transform` defined then title will be determined by this
    - If `entry_folder_name` defined then title will use the same value
    - If `collection_name` defined then title will use the same value
- `episode` - If the mapping has an `episode_transform` defined then the new episode number will be determined by this, otherwise taken as is from the original filename
- `resolution` - Taken as is from the original filename
- `extension` - Taken as is from the original filename

## Docker

`manager` can be run inside a docker container to constantly monitor a chosen `source` folder for file changes and automatically run it. Simply use the following command to start running it.

    docker run \
    -e PUID=<PUID> \
    -e PGID=<PGID> \
    -e UMASK=<UMASK> \
    -e CLEAR=<CLEAR> \
    -e HOT_RELOAD=<HOT_RELOAD> \
    -e SKIP_HALF_EPISODES=<SKIP_HALF_EPISODES> \
    -e DELETE_HALF_EPISODES=<DELETE_HALF_EPISODES> \
    -v "<source>:/source" \
    -v "<destination>:/destination" \
    -v "<config>:/config" \
    jovaee/anime-scripts:manager

* `<PUID>` - User ID to run as
* `<PGID>` - User Group ID to run as
* `<UMASK>` - (Optional) umask for newly created folders. Default is `002` but would probably want this as `000` if `PUID` is not default user so that other users have write permissions.
* `<CLEAR>` - (Optional) Default `false`. Delete any empty directories in `source` after each run
* `<HOT_RELOAD>` - (Optional) Default `false`. Automatically copies the `mappings.py` from the `<config>` directory into the running directory whenever a change was made to the file.
* `<SKIP_HALF_EPISODES>` - (Optional) Default `false`. Half episodes will be skipped and not moved to the `destination` folder.
* `<DELETE_HALF_EPISODES>` - (Optional) Default `false`. Half episodes will be skipped and the file deleted. If both `SKIP_HALF_EPISODES` and `DELETE_HALF_EPISODES` specified, `SKIP_HALF_EPISODES` will take precedence.
* `<source>` - Is the folder that will be monitored for changes
* `<destination>` - Is the folder where `manager` will output processed files to
* `<config>` - Is the folder where a custom `mappings.py` file must be put. Every time `mappings.py` is changed the docker container needs to be restarted for the changes to be reflected unless hot reload is enabled.

## Validation

### Manual

A validation script is included that will make sure that mappings are defined correctly. It comes with two predefined
rulesets `FULL` and `SPARSE` that can be chosen from when running the script.

`SPARSE`
* Required fields are defined in the mapping
* Mapping field types are correct
* Episode ranges are correct:
    * Start value is `1` - episode transformation taking into account

`FULL` (Same as `SPARSE` but also with)
* Episode ranges are correct:
    * End value matches with AniDB episode count - episode transformation taking into account
* Checks for "old" mappings where anime have completed at least 60 days ago

This script can be run directly in the running docker container, but a few additional environment variables will have 
to be set

* `<ANIDB_CLIENT_NAME>` - Client name of your Anidb client
* `<ANIDB_CLIENT_VERSION>` - Client version of your Anidb client

Run it by using
```
python validate.py
```

### Automatic

Before a mapping is applied to a file it is first validated with the `SPARSE` ruleset. If the validation fails the mapping
will not be applied to the file. The validation feedback will be logged.
