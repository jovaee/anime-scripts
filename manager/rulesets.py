"""
Defined rulesets to run over mappings
"""
from types import FunctionType

from mappingschema.base import Node, RuleSet
from mappingschema.rules import FieldTypeRule, FieldRequiredRule, FieldLengthRule, OldMappingRule, EpisodeRangeOfficialRule

_COMMON = [
    RuleSet(
        "collection_name",
        [
            FieldTypeRule((str)),
            FieldRequiredRule(),
        ]
    ),
    RuleSet(
        "required_episode_title",
        [
            FieldTypeRule((str)),
        ]
    ),
    RuleSet(
        "entry_folder_name",
        [
            FieldTypeRule((str)),
        ]
    ),
    RuleSet(
        "title_transform",
        [
            FieldTypeRule((str, FunctionType)),
        ]
    ),
    RuleSet(
        "episode_transform",
        [
            FieldTypeRule((FunctionType)),
        ]
    ),
    RuleSet(
        "other",
        [
            FieldTypeRule((bool)),
        ]
    ),
]

FULL = RuleSet(
    Node(),
    [
        *_COMMON,
        RuleSet(
            "anidb_id",
            [
                FieldTypeRule((int)),
                FieldRequiredRule(),
                OldMappingRule(),
            ]
        ),
        RuleSet(
            "required_episode_range",
            [
                FieldTypeRule((list, set, tuple)),
                FieldRequiredRule(),
                FieldLengthRule(2),
                RuleSet(
                    0,
                    [
                        FieldTypeRule((int, float))
                    ],
                ),
                RuleSet(
                    1,
                    [
                        FieldTypeRule((int, float))
                    ],
                ),
                EpisodeRangeOfficialRule()
            ]
        ),
    ],
    name="FULL"
)

SPARSE = RuleSet(
    Node(),
    [
        *_COMMON,
        RuleSet(
            "anidb_id",
            [
                FieldTypeRule((int)),
                FieldRequiredRule(),
            ]
        ),
        RuleSet(
            "required_episode_range",
            [
                FieldTypeRule((list, set, tuple)),
                FieldRequiredRule(),
                FieldLengthRule(2),
                RuleSet(
                    0,
                    [
                        FieldTypeRule((int, float))
                    ],
                ),
                RuleSet(
                    1,
                    [
                        FieldTypeRule((int, float))
                    ],
                ),
            ]
        ),
    ],
    name="SPARSE",
)
