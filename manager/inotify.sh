#!/bin/sh
set -e

echo "|===============================|"
echo "|         Anime Manager         |"
echo "|===============================|"

DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

[ ! -d "source/" ] && echo "Directory source/ does not exists." && exit 1
[ ! -d "destination/" ] && echo "Directory destination/ does not exists." && exit 1
[ ! -d "config/" ] && echo "Directory config/ does not exists." && exit 1
[ ! -f "config/mappings.py" ] && echo "Missing custom mappings.py file in config/" && exit 1

echo "Setting up user permissions..."
chown -R $PUID manager/
chgrp -R $PGID manager/
echo "Running as user $PUID and group $PGID"

echo "Copying custom mappings..."
LAST_MAPPINGS_CHANGE=$(date +%s)
cp -f config/mappings.py manager/mappings.py

inotifywait -r -m source/ -e close_write -e moved_to |
    while read path action file; do
        echo "$action - $path$file"

        # Check if the mappings file has been changed since the last time it has been copied
        if [[ ! -z $HOT_RELOAD && $HOT_RELOAD = true && $(date -r config/mappings.py +%s) -ge $LAST_MAPPINGS_CHANGE ]]; then
            echo "Custom mappings have changed, copying file..."
            LAST_MAPPINGS_CHANGE=$(date +%s)
            cp -f config/mappings.py manager/mappings.py
        fi

        # -E preserve env variables under other user
        # -u user to run command as
        # -g user group to run command as
        sudo -E -u "#$PUID" -g "#$PGID" python3 manager/main.py -s source/ -d destination/

        if [[ ! -z $CLEAR && $CLEAR = true ]]; then
            # Delete any empty directoies in source/
            find source/ -type d -empty -delete
        fi
    done