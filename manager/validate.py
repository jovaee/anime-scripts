"""
Validate mappings.py
"""
import click
import importlib
import os

from pprint import pprint

from anidb import get_official_end_date, get_official_episode_count
from mappings import MAPPINGS
from mappingschema.base import RuleSet


CLIENT_NAME = os.environ.get("ANIDB_CLIENT_NAME")
CLIENT_VERSION = os.environ.get("ANIDB_CLIENT_VERSION", 1)


def validate_mapping(ruleset: RuleSet, mapping: dict) -> dict:
    anidb_id = mapping.get("anidb_id")
    anidb_episode_count = None
    anidb_date_end = None

    # Only get the AniDB data if we are running the rules that require it
    if ruleset.name == "FULL" and isinstance(anidb_id, int):
        anidb_episode_count = get_official_episode_count(anidb_id)
        anidb_date_end = get_official_end_date(anidb_id)

    return ruleset.validate(mapping, anidb_episode_count=anidb_episode_count, anidb_date_end=anidb_date_end)


@click.command("validate")
@click.option(
    "--ruleset",
    default="SPARSE",
    type=click.Choice(("FULL", "SPARSE")),
    help="""
    Predefined rulesets to use to validate all mappings.
    SPARSE: Basic set of rules to ensures mappings are defined correctly.
    FULL: Same as SPARSE but includes AniDB verification checks.
    """
)
def main(ruleset: str):
    # Dynamically import and get the given ruleset from the predefined ones
    rulesets = importlib.import_module("rulesets")
    
    for index, mapping in enumerate(MAPPINGS):
        feedback = validate_mapping(getattr(rulesets, ruleset), mapping)
        if feedback:
            pprint({f"#{mapping.get('anidb_id', f'index-{index}')}": feedback}, indent=2, width=240)


if __name__ == "__main__":
    main()
