from collections import defaultdict
from typing import Any, Union


class Empty:
    """
    Empty field. When the given field is not present in the data
    """

    def __repr__(self) -> str:
        return "Empty"


class Node:

    def __repr__(self) -> str:
        return "Node"


class Rule:
    """
    """

    field: int | str

    def validate(self, value: Any | Empty, **kwargs) -> str:
        raise NotImplementedError


class RuleSet:
    """
    """

    def __init__(self, field: int | str | Empty, rules: list[Union[Rule, "RuleSet"]], name: str | None = None):
        """
        :param field: The field to validate
        :param rules: The rules to run on the given field
        :param name: Optional name for the ruleset. Doesn't affect any functionality
        """
        self.field = field
        self.rules = rules

        self.name = name

        for rule in self.rules:
            if isinstance(rule, Rule):
                rule.field = field

    def validate(self, value: Any, **kwargs) -> dict:
        feedbacks = defaultdict(list)

        params = {**kwargs, **{"mapping": value}}
        for rule in self.rules:
            if isinstance(self.field, Node):
                # If it's a Node then no validate should be done on it directly
                # Just go down the chain and validate the rest
                feedback = rule.validate(value, **params)
            elif hasattr(value, "get"):
                # Dict
                feedback = rule.validate(value.get(self.field, Empty()), **params)
            elif hasattr(value, "index"):
                # List
                # Make sure to not gou out of bounds
                value = value[self.field] if self.field < len(value) else Empty()
                feedback = rule.validate(value, **params)
            else:
                # Some primitive type... hopefully
                feedback = rule.validate(value, **params)

            if feedback:
                feedbacks[self.field].append(feedback)

        return dict(feedbacks)
