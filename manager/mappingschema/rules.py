from datetime import datetime
from typing import Any

from .base import Empty, Rule


class FieldRequiredRule(Rule):
    """
    Field is required
    """

    def validate(self, value: Any | Empty, **kwargs) -> str:
        if isinstance(value, Empty):
            return "Required field in a mapping"


class FieldTypeRule(Rule):
    """
    Field should be of specific type
    """

    def __init__(self, types: list[Any]):
        self.types = types

    def validate(self, value: Any | Empty, **kwargs) -> str:
        if not isinstance(value, Empty) and not isinstance(value, self.types):
            return f"Required to be one of the following types {self.types}"


class FieldLengthRule(Rule):
    """
    Field cannot exceed a certain length
    """

    def __init__(self, length: int):
        self.length = length

    def validate(self, value: Any | Empty, **kwargs) -> str:
        if (
            not isinstance(value, Empty)
            and (
                not hasattr(value, "__len__")
                or len(value) != self.length
            )
        ):
            return f"Required to have length {self.length}"


class EpisodeRangeOfficialRule(Rule):
    """
    Episode range should be valid
    """

    def validate(self, value: Any | Empty, mapping: dict, anidb_episode_count: int | None, **kwargs) -> str:
        # Some rule crossing validation but required since rules don't know which ones might have passed
        if not isinstance(value, Empty) and hasattr(value, "__len__") and len(value) == 2:
            start, end = value[0], value[1]
            if isinstance(start, (int, float)) and isinstance(end, (int, float)):
                if end <= start:
                    return "Episode start value should be smaller than episode end value"
                if start % 1 not in (0, 0.5):
                    return "Episode start range should be a multiple of 0.5"
                if end % 1 not in (0, 0.5):
                    return "Episode end range should be a multiple of 0.5"

                episode_transform = lambda x: x
                if "episode_transform" in mapping:
                    episode_transform = mapping["episode_transform"]
                
                # Compare with anidb episode data
                transformed_start = float(episode_transform(start))
                transformed_end = float(episode_transform(end))

                if transformed_start != 1:
                    return "Episode start value should be 1 after transformation"
                if anidb_episode_count is not None and anidb_episode_count != transformed_end:
                    return f"Episode end value is not the same as the official episode count of {anidb_episode_count}"


class OldMappingRule(Rule):
    """
    Mapping cannot exceed a certain age after end date
    """

    def validate(self, value: Any | Empty, anidb_date_end: datetime | None, **kwargs) -> str:
        if (
            not isinstance(value, Empty)
            and isinstance(value, int)
            and anidb_date_end
            and (datetime.now() - anidb_date_end).total_seconds() > (60 * 60 * 24 * 30)  # Older than 30 days after completion date

        ):
            return "Mapping quite old, consider removing it"
